package advanceProgramming;

import java.awt.Color;
import java.awt.Graphics;

public class picture {
	public void drawGridLines1(Graphics g) {
        for (int are = 0; are <= 7; are++) {
          g.drawLine(20, 20 + are * 20, 180, 20 + are * 20);
        }
	}
  	
    
    public void drawGridLines(Graphics g) {
      g.setColor(Color.LIGHT_GRAY);
      for (int are = 0; are <= 7; are++) {
    	  g.drawLine(20, 20 + are * 20, 180, 20 + are * 20);
      }
    }

    
    public void drawGridLines2(Graphics g) {
        g.setColor(Color.LIGHT_GRAY);
        drawGridLines1(g);
        drawGridLines2(g);
    }
    
}
